<img src="pluginIcon.png" alt="Darcula minimal dark theme" width="90"/>

## The mimial Darcula dark theme for Jetbrains
##### Mimimal all dark colors
<img src="demo/code-editor.png" alt="Darcula minimal dark theme" width="2000"/>

##### Mimial all lines and borders dark colors
<img src="demo/line-colors.png" alt="Darcula minimal dark theme" width="2000"/>

##### Highlighted matched brackets
<img src="demo/matched-brackets.png" alt="Darcula minimal dark theme" width="2000"/>

### Installation
- Install PluginDevKit
- Update what you need in plugin.xml or DarculaMinimalDark.theme.json or DarculaMinimalDark.xml
- Build deploy .zip: right click on project -> Prepare Plugin Module....
- Login to Jetbrains account and Install plugin from Marketplace

### Others
- Hide template HTML background: ![alt text](image.png)
